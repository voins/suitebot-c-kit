#!/bin/sh
cd `dirname $0`
git submodule update --init
mkdir -p build
cd build
CC=gcc-5 CXX=g++-5 cmake ../
make

