#include <game/builder.hh>

game::builder::builder(const std::string& src, int energy) {
    int x = 0;
    for(char c: src) {
        if(c == '*')      add_obstacle(point {x, height});
        else if(c == '!') add_treasure(point {x, height});
        else if(c == '+') add_battery(point {x, height});
        else if(isdigit(c)) add_bot(c - '0', point {x, height}, energy);
        else if(c == '\n') {
            if(!width) width = x;
            else if(width != x) throw std::runtime_error("invalid state");
            ++height; x = -1;
        } else if(c != ' ') throw std::runtime_error("invalid state");
        ++x;
    }
}
