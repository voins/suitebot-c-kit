#include <net/server.hh>
#include <net/listener.hh>
#include <string>
#include <istream>

namespace {
    std::string& trim_cr(std::string& request) {
        if(request.back() == '\xd')
            request.erase(request.size() - 1);
        return request;
    }
}

void net::server::run(void) {
    net::listener listener(port);

    while(!handler.exit_requested()) {
        net::socketbuf reader = listener.accept();
        std::iostream client(&reader);
        handle(client);
    }
}

bool net::server::handle(std::iostream& client) {
    try {
        std::string request;
        std::getline(client, request);
        if(client)
            client << handler.process(trim_cr(request)) << std::endl;
    } catch (std::exception& e) {
        client << "error: " << e.what() << std::endl;
    }
    return false;
}
