#include <json/parser.hh>

namespace json {
    std::shared_ptr<token> parser::fetch(void) {
        skip_whitespace();
        if(atend())                   return std::make_shared<end_token>();
        else if(is('\"'))             return parse_string();
        else if(is("t"))              return parse_true();
        else if(is("f"))              return parse_false();
        else if(is("n"))              return parse_null();
        else if(in("[]{}"))           return parse_mark();
        else if(isdigit() || is('-')) return parse_number();
        else                          throw_error();
    }

    void parser::skip(std::shared_ptr<token> t) {
        int depth = 0;
        if(t->is_object()) {
            for(;!t->is_end(); t = fetch()) {
                if(t->is_object()) depth++;
                else if(t->is_object_end()) depth--;
                if(!depth) return;
            }
        } else if(t->is_array()) {
            for(;!t->is_end(); t = fetch()) {
                if(t->is_array()) depth++;
                else if(t->is_array_end()) depth--;
                if(!depth) return;
            }
        }
    }

    [[noreturn]] void parser::throw_error(void) {
        throw std::runtime_error("parse error");
    }

    char parser::parse_escape(void) {
        advance();
        if(atend()) throw_error();

        switch(current()) {
        case '\"': return '\"';
        case '/':  return '/';
        case '\\': return '\\';
        case 'b':  return '\b';
        case 'f':  return '\f';
        case 'r':  return '\r';
        case 'n':  return '\n';
        case 't':  return '\t';
        case 'u':  return parse_hexcode();
        default:   throw_error();
        }
    }

    char parser::parse_hexcode(void) {
        std::string code;
        for(int i = 0; i < 4; ++i) {
            advance();
            if(!isxdigit()) throw_error();
            code += current();
        }
        return std::stoi(code, nullptr, 16);
    }

    void parser::parse_word(const std::string& word) {
        if(!is(word)) throw_error();
        advance(word.size());
    }

    std::shared_ptr<token> parser::parse_string(void) {
        std::string acc;

        advance();
        while(!atend() && !is('\"')) {
            acc += is('\\') ? parse_escape() : current();
            advance();
        }
        if(atend()) throw_error();

        advance();
        return std::make_shared<string_token>(acc);
    }

    std::shared_ptr<token> parser::parse_number(void) {
        std::string acc;

        while(isdigit() || in(".eE+-")) {
            acc += current();
            advance();
        }

        size_t index;
        auto int_result = stoll(acc, &index);
        if(index == acc.size())
            return std::make_shared<integer_token>(int_result);

        auto double_result = stod(acc, &index);
        if(index == acc.size())
            return std::make_shared<double_token>(double_result);

        throw std::runtime_error("parse error");
    }

    std::shared_ptr<token> parser::parse_mark(void) {
        char c = current();
        advance();
        return std::make_shared<mark_token>(c);
    }

    std::shared_ptr<token> parser::parse_true(void) {
        parse_word("true");
        return std::make_shared<boolean_token>(true);
    }

    std::shared_ptr<token> parser::parse_false(void) {
        parse_word("false");
        return std::make_shared<boolean_token>(false);
    }

    std::shared_ptr<token> parser::parse_null(void) {
        parse_word("null");
        return std::make_shared<null_token>();
    }
}
