#include <suitebot/deserialize.hh>

namespace suitebot {
    deserialize::deserialize(const std::string& request): p(request) {
        if(p.fetch()->is_object()) {
            for(auto key = p.fetch(); !key->is_object_end(); key = p.fetch()) {
                auto value = p.fetch();
                if(key->as_string() == "yourBotId")
                    set_your_bot_id(value);
                else if(key->as_string() == "gamePlan")
                    set_game_plan(value);
                else if(key->as_string() == "botIds")
                    set_bot_ids(value);
                else if(key->as_string() == "botEnergyMap")
                    set_bot_energy(value);
                p.skip(value);
            }
        }
    }

    void deserialize::set_your_bot_id(std::shared_ptr<json::token>& value) {
        if(value->is_integer())
            yourbotid = value->as_integer();
    }

    void deserialize::set_game_plan(std::shared_ptr<json::token>& value) {
        if(value->is_array()) {
            for(value = p.fetch(); !value->is_array_end(); value = p.fetch()) {
                if(value->is_string())
                    game_state += value->as_string() + "\n";
                p.skip(value);
            }
        }
    }

    void deserialize::set_bot_ids(std::shared_ptr<json::token>& value) {
        if(value->is_array()) {
            for(value = p.fetch(); !value->is_array_end(); value = p.fetch()) {
                if(value->is_integer())
                    bot_ids.insert(value->as_integer());
                p.skip(value);
            }
        }
    }

    void deserialize::set_bot_energy(std::shared_ptr<json::token>& token) {
        if(token->is_object()) {
            for(token = p.fetch(); !token->is_object_end(); token = p.fetch()) {
                auto value = p.fetch();
                if(value->is_string())
                    energy_map.insert(std::make_pair(stoi(token->as_string()),
                                                     value->as_integer()));
                p.skip(value);
            }
        }
    }
}
