#include <suitebot/sample-ai.hh>
#include <list>

namespace {
    using game::point;
    using game::state;

    struct move {
        point step;
        std::string name;
    };

    inline move operator +(const move& a, const move& b) {
        return move {a.step + b.step, a.name + b.name};
    }

    move U {{0, -1}, "U"};
    move D {{0, +1}, "D"};
    move L {{-1, 0}, "L"};
    move R {{+1, 0}, "R"};

    class solver {
        static std::list<move> basic_moves;
        const point& me;
        const state& field;
        std::list<move> single_moves;
        std::list<move> double_moves;
        std::list<move> candidates;

    public:
        solver(const point& me, const state& field): me(me), field(field) {
            select_safe_moves();
            moves_to_treasure(single_moves);
            moves_to_battery(single_moves);
            moves_to_treasure(double_moves);
            moves_to_battery(double_moves);
            candidates.push_back(D);
        }

        move get_move(void) {
            return candidates.front();
        }

    private:
        void select_safe_moves(void) {
            for(auto x: basic_moves) {
                if(field.obstacle_locations().count(me + x.step)) continue;
                single_moves.push_back(x);
                for(auto y: basic_moves)
                    if(!field.obstacle_locations().count(me + x.step + y.step))
                        double_moves.push_back(x + y);
            }
        }

        void moves_to_treasure(const std::list<move>& moves) {
            for(auto x: moves)
                if(field.treasure_locations().count(me + x.step))
                    candidates.push_back(x);
        }

        void moves_to_battery(const std::list<move>& moves) {
            for(auto x: moves)
                if(field.battery_locations().count(me + x.step))
                    candidates.push_back(x);
        }
    };

    std::list<move> solver::basic_moves {U, D, L, R};
}

namespace suitebot {
    std::string
    sample_ai::make_move(int id, std::shared_ptr<game::state> state) {
        return solver(state->bot_location(id), *state).get_move().name;
    }
}
