#include <iostream>
#include <net/server.hh>
#include <suitebot/sample-ai.hh>
#include <suitebot/request-handler.hh>

const int DEFAULT_PORT = 9001;

int determinePort(int argc, char *argv[]) {
    if(argc == 2)
        return std::stoi(argv[1]);
    else
        return DEFAULT_PORT;
}

int main(int argc, char *argv[]) {
    int port = determinePort(argc, argv);

    std::cout << "listening on port " << port << std::endl;

    suitebot::sample_ai bot_ai;
    suitebot::request_handler handler(bot_ai);
    net::server server(port, handler);

    server.run();

    return 0;
}
