#include <gmock/gmock.h>
#include <json/parser.hh>

using namespace ::testing;

class json_parser_integer_test
    : public TestWithParam<std::pair<const char *, int> > {
};

TEST_P(json_parser_integer_test, number_string__integer) {
    json::parser p(GetParam().first);

    auto result = p.fetch();

    ASSERT_TRUE(result->is_integer());
    ASSERT_EQ(result->as_integer(), GetParam().second);
}

INSTANTIATE_TEST_CASE_P(params, json_parser_integer_test,
                        Values(std::make_pair("1234",  1234),
                               std::make_pair("-5678", -5678)));
