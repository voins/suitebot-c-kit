;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c++-mode
  (flycheck-clang-language-standard . "c++11")
  (flycheck-gcc-language-standard . "c++11")
  (flycheck-clang-include-path . ("../include"
                                  "../googletest/googletest/include"
                                  "../googletest/googlemock/include"))
  (flycheck-gcc-include-path . ("../include"
                                "../googletest/googletest/include"
                                "../googletest/googlemock/include"))))

