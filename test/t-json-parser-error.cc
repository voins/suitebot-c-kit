#include <gmock/gmock.h>
#include <json/parser.hh>

using namespace ::testing;

class json_parser_error_test
    : public TestWithParam<const char *> {
};

TEST_P(json_parser_error_test, invalid_input__error_thrown) {
    json::parser p(GetParam());

    ASSERT_THROW(p.fetch(), std::runtime_error);
}

INSTANTIATE_TEST_CASE_P(params, json_parser_error_test,
                        Values("\"", "\"\\", "\"\\l\"", "\"\\ur\"",
                               "ttt", "fff", "nnn", "123.5.6", "()"));
