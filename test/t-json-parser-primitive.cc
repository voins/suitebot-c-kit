#include <gmock/gmock.h>
#include <json/parser.hh>

using namespace ::testing;

TEST(json_parser_primitive_test, true_string__boolean_true) {
    json::parser p("true");

    auto result = p.fetch();

    ASSERT_TRUE(result->is_boolean());
    ASSERT_EQ(result->as_boolean(), true);
}

TEST(json_parser_primitive_test, false_string__boolean_false) {
    json::parser p("false");

    auto result = p.fetch();

    ASSERT_TRUE(result->is_boolean());
    ASSERT_EQ(result->as_boolean(), false);
}

TEST(json_parser_primitive_test, null_string__null_token) {
    json::parser p("null");

    auto result = p.fetch();

    ASSERT_TRUE(result->is_null());
}
