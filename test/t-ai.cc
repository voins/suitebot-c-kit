#include <gmock/gmock.h>
#include <suitebot/sample-ai.hh>
#include <game/builder.hh>
#include <game/immutable_state.hh>

using namespace ::testing;

TEST(ai, move_to_treasure_one_step) {
    suitebot::sample_ai ai;
    auto state =
        std::make_shared<game::immutable_state>(game::builder(" ! \n"
                                                              "+1 \n"
                                                              "   \n"));


    auto result = ai.make_move(1, state);

    ASSERT_EQ(result, "U");
}

TEST(ai, move_to_battery_one_step) {
    suitebot::sample_ai ai;
    auto state =
        std::make_shared<game::immutable_state>(game::builder("   \n"
                                                              "+1 \n"
                                                              "   \n"));


    auto result = ai.make_move(1, state);

    ASSERT_EQ(result, "L");
}

TEST(ai, no_treasure_or_battery__down) {
    suitebot::sample_ai ai;
    auto state =
        std::make_shared<game::immutable_state>(game::builder("   \n"
                                                              " 1 \n"
                                                              "   \n"));


    auto result = ai.make_move(1, state);

    ASSERT_EQ(result, "D");
}

TEST(ai, move_to_treasure_two_steps) {
    suitebot::sample_ai ai;
    auto state =
        std::make_shared<game::immutable_state>(game::builder("! +\n"
                                                              " 1 \n"
                                                              "   \n"));


    auto result = ai.make_move(1, state);

    ASSERT_EQ(result, "UL");
}

TEST(ai, move_to_treasure_two_steps_obstacle) {
    suitebot::sample_ai ai;
    auto state =
        std::make_shared<game::immutable_state>(game::builder("!* \n"
                                                              " 1 \n"
                                                              "   \n"));


    auto result = ai.make_move(1, state);

    ASSERT_EQ(result, "LU");
}

TEST(ai, move_to_battery_two_steps) {
    suitebot::sample_ai ai;
    auto state =
        std::make_shared<game::immutable_state>(game::builder("  +\n"
                                                              " 1 \n"
                                                              "   \n"));


    auto result = ai.make_move(1, state);

    ASSERT_EQ(result, "UR");
}

TEST(ai, move_to_battery_two_steps_obstacle) {
    suitebot::sample_ai ai;
    auto state =
        std::make_shared<game::immutable_state>(game::builder(" *+\n"
                                                              " 1 \n"
                                                              "   \n"));


    auto result = ai.make_move(1, state);

    ASSERT_EQ(result, "RU");
}

TEST(ai, choose_battery_two_steps_treasure_hidden) {
    suitebot::sample_ai ai;
    auto state =
        std::make_shared<game::immutable_state>(game::builder(" !  \n"
                                                              " *  \n"
                                                              " 1 +\n"
                                                              "    \n"));

    auto result = ai.make_move(1, state);

    ASSERT_EQ(result, "RR");
}
