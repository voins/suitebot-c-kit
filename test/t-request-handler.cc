#include <gmock/gmock.h>
#include <suitebot/request-handler.hh>
#include <test/ai.hh>
#include <test/timer.hh>

using namespace ::testing;

struct request_handler_fake_parse: public suitebot::request_handler {
    request_handler_fake_parse(suitebot::ai& ai)
        : suitebot::request_handler(ai)
    {}

    MOCK_METHOD1(parse, suitebot::parsed_request(const std::string&));
};

struct fake_state: public game::state {
    MOCK_CONST_METHOD0(plan_width, int(void));
    MOCK_CONST_METHOD0(plan_height, int(void));
    MOCK_CONST_METHOD0(all_bot_ids, const std::unordered_set<int>&(void));
    MOCK_CONST_METHOD0(live_bot_ids, const std::unordered_set<int>&(void));
    MOCK_CONST_METHOD1(bot_location, const game::point&(int));
    MOCK_CONST_METHOD1(bot_energy, int(int));
    MOCK_CONST_METHOD0(obstacle_locations,
                       const std::unordered_set<game::point>&(void));
    MOCK_CONST_METHOD0(treasure_locations,
                       const std::unordered_set<game::point>&(void));
    MOCK_CONST_METHOD0(battery_locations,
                       const std::unordered_set<game::point>&(void));
};

TEST(request_handler, exit_request__exit_flag_set) {
    fake::ai ai;
    suitebot::request_handler handler(ai);

    handler.process("EXIT");

    ASSERT_TRUE(handler.exit_requested());
}


TEST(request_handler, name_request__ai_name_returned) {
    fake::ai ai;
    suitebot::request_handler handler(ai);

    EXPECT_CALL(ai, name()).WillOnce(Return("fake ai"));

    auto result = handler.process("NAME");

    ASSERT_EQ(result, "fake ai");
}


TEST(request_handler, uptime_request__timer_called) {
    fake::ai ai;
    fake::timer timer;

    EXPECT_CALL(timer, now())
        .WillOnce(Return(10))
        .WillOnce(Return(20));
    suitebot::request_handler handler(ai, timer);

    auto result = handler.process("UPTIME");

    ASSERT_EQ(result, "10");
}

TEST(request_handler, other_string__parsed_and_ai_called) {
    fake::ai ai;
    request_handler_fake_parse handler(ai);

    std::shared_ptr<game::state> state
        = std::make_shared<fake_state>();
    EXPECT_CALL(handler, parse("{}"))
        .WillOnce(Return(suitebot::parsed_request {0, state}));
    EXPECT_CALL(ai, make_move(0, state))
        .WillOnce(Return("result"));

    auto result = handler.process("{}");

    ASSERT_EQ(result, "result");
}
