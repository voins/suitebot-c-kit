#include <gmock/gmock.h>
#include <json/parser.hh>

using namespace ::testing;

class json_parser_string_test
    : public TestWithParam<std::pair<const char *, const char *> > {
};

TEST_P(json_parser_string_test, source_string__parsed_string) {
    json::parser p(GetParam().first);

    auto result = p.fetch();

    ASSERT_TRUE(result->is_string());
    ASSERT_EQ(result->as_string(), GetParam().second);
}

INSTANTIATE_TEST_CASE_P(params, json_parser_string_test,
                        Values(std::make_pair("\"result\"",  "result"),
                               std::make_pair("\"\\\"\"",    "\""),
                               std::make_pair("\"\\/\"",     "/"),
                               std::make_pair("\"\\\\\"",    "\\"),
                               std::make_pair("\"\\b\"",     "\b"),
                               std::make_pair("\"\\f\"",     "\f"),
                               std::make_pair("\"\\r\"",     "\r"),
                               std::make_pair("\"\\n\"",     "\n"),
                               std::make_pair("\"\\t\"",     "\t"),
                               std::make_pair("\"\\u0040\"", "@")));
