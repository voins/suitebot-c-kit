#include <gmock/gmock.h>
#include <json/parser.hh>

using namespace ::testing;

class json_parser_double_test
    : public TestWithParam<std::pair<const char *, double> > {
};

TEST_P(json_parser_double_test, number_string__double) {
    json::parser p(GetParam().first);

    auto result = p.fetch();

    ASSERT_TRUE(result->is_double());
    ASSERT_EQ(result->as_double(), GetParam().second);
}

INSTANTIATE_TEST_CASE_P(params, json_parser_double_test,
                        Values(std::make_pair("1234.5",  1234.5),
                               std::make_pair("1234.5 ", 1234.5),
                               std::make_pair("-5678.9", -5678.9),
                               std::make_pair("12.56E+2", 1256.0),
                               std::make_pair("12.56E-2", 0.1256)));
