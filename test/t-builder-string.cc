#include <gmock/gmock.h>
#include <game/builder.hh>
#include <game/immutable_state.hh>

using namespace ::testing;

TEST(builder_string, empty_map_2x2) {
    game::builder b("  \n"
                    "  \n");

    ASSERT_EQ(b.width, 2);
    ASSERT_EQ(b.height, 2);
    ASSERT_TRUE(b.obstacles.empty());
    ASSERT_TRUE(b.treasures.empty());
    ASSERT_TRUE(b.batteries.empty());
    ASSERT_TRUE(b.bot_ids.empty());
    ASSERT_TRUE(b.bots.empty());
    ASSERT_TRUE(b.energy.empty());
}

TEST(builder_string, normal_map_4x3) {
    game::builder b("1*+ \n"
                    "+!3*\n"
                    " 2! \n");

    ASSERT_EQ(b.width, 4);
    ASSERT_EQ(b.height, 3);
    ASSERT_THAT(b.obstacles, UnorderedElementsAre(game::point {1, 0},
                                                  game::point {3, 1}));
    ASSERT_THAT(b.treasures, UnorderedElementsAre(game::point {1, 1},
                                                  game::point {2, 2}));
    ASSERT_THAT(b.batteries, UnorderedElementsAre(game::point {2, 0},
                                                  game::point {0, 1}));
    ASSERT_THAT(b.bot_ids, UnorderedElementsAre(1, 2, 3));
    ASSERT_THAT(b.bots,
                UnorderedElementsAre(std::make_pair(1, game::point {0, 0}),
                                     std::make_pair(2, game::point {1, 2}),
                                     std::make_pair(3, game::point {2, 1})));
    ASSERT_THAT(b.energy,
                UnorderedElementsAre(std::make_pair(1, 10),
                                     std::make_pair(2, 10),
                                     std::make_pair(3, 10)));
}

TEST(builder_string, normal_map_4x3_with_state) {
    game::builder b("1*+ \n"
                    "+!3*\n"
                    " 2! \n");
    game::immutable_state s(b);

    ASSERT_EQ(s.plan_width(), 4);
    ASSERT_EQ(s.plan_height(), 3);
    ASSERT_THAT(s.obstacle_locations(),
                UnorderedElementsAre(game::point {1, 0},
                                     game::point {3, 1}));
    ASSERT_THAT(s.treasure_locations(),
                UnorderedElementsAre(game::point {1, 1},
                                     game::point {2, 2}));
    ASSERT_THAT(s.battery_locations(),
                UnorderedElementsAre(game::point {2, 0},
                                     game::point {0, 1}));
    ASSERT_THAT(s.all_bot_ids(), UnorderedElementsAre(1, 2, 3));
    ASSERT_EQ(s.bot_location(1), (game::point {0, 0}));
    ASSERT_EQ(s.bot_location(2), (game::point {1, 2}));
    ASSERT_EQ(s.bot_location(3), (game::point {2, 1}));
    ASSERT_EQ(s.bot_energy(1), 10);
    ASSERT_EQ(s.bot_energy(2), 10);
    ASSERT_EQ(s.bot_energy(3), 10);
}
