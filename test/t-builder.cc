#include <gmock/gmock.h>
#include <game/builder.hh>

using namespace ::testing;

TEST(builder, set_width__sets_width) {
    game::builder b;

    b.set_width(10);

    ASSERT_EQ(b.width, 10);
}

TEST(builder, set_height__sets_height) {
    game::builder b;

    b.set_height(10);

    ASSERT_EQ(b.height, 10);
}

TEST(builder, add_obstacle__adds_obstacles) {
    game::builder b;

    b.add_obstacle(game::point {5, 3});

    ASSERT_THAT(b.obstacles, ElementsAre(game::point {5, 3}));
}

TEST(builder, add_tresure__adds_trasure) {
    game::builder b;

    b.add_treasure(game::point {2, 8});

    ASSERT_THAT(b.treasures, ElementsAre(game::point {2, 8}));
}

TEST(builder, add_battery__adds_battery) {
    game::builder b;

    b.add_battery(game::point {4, 7});

    ASSERT_THAT(b.batteries, ElementsAre(game::point {4, 7}));
}

TEST(builder, add_bot__adds_bot_and_energy) {
    game::builder b;

    b.add_bot(1, game::point {2, 3}, 4);

    ASSERT_THAT(b.bot_ids, ElementsAre(1));
    ASSERT_THAT(b.bots, ElementsAre(std::make_pair(1, game::point {2, 3})));
    ASSERT_THAT(b.energy, ElementsAre(std::make_pair(1, 4)));
}

TEST(builder, set_energy_new_bot__appends_value) {
    game::builder b;

    b.set_energy(1, 2);

    ASSERT_THAT(b.energy, ElementsAre(std::make_pair(1, 2)));
}

TEST(builder, set_energy_existing_bot__change_value) {
    game::builder b;

    b.set_energy(1, 2);
    b.set_energy(1, 3);

    ASSERT_THAT(b.energy, ElementsAre(std::make_pair(1, 3)));
}
