#include <gmock/gmock.h>
#include <json/parser.hh>

using namespace ::testing;

TEST(json_parser, empty__end) {
    json::parser p("");

    auto result = p.fetch();

    ASSERT_TRUE(result->is_end());
}

TEST(json_parser, whitespace__end) {
    json::parser p(" \t\n\r");

    auto result = p.fetch();

    ASSERT_TRUE(result->is_end());
}

TEST(json_parser, skip_number__token_after) {
    json::parser p("123 456");

    p.skip(p.fetch());
    auto result = p.fetch();

    ASSERT_EQ(result->as_integer(), 456);
}

TEST(json_parser, skip_empty_array__token_after) {
    json::parser p("[] 456");

    p.skip(p.fetch());
    auto result = p.fetch();

    ASSERT_EQ(result->as_integer(), 456);
}

TEST(json_parser, skip_array__token_after) {
    json::parser p("[123]456");

    p.skip(p.fetch());
    auto result = p.fetch();

    ASSERT_EQ(result->as_integer(), 456);
}

TEST(json_parser, skip_empty_object__token_after) {
    json::parser p("{} 456");

    p.skip(p.fetch());
    auto result = p.fetch();

    ASSERT_EQ(result->as_integer(), 456);
}

TEST(json_parser, skip_object__token_after) {
    json::parser p("{\"aaa\":123}456");

    p.skip(p.fetch());
    auto result = p.fetch();

    ASSERT_EQ(result->as_integer(), 456);
}
