#include <gmock/gmock.h>
#include <json/parser.hh>

using namespace ::testing;

TEST(json_parser_marker_test, open_curly__object_mark) {
    json::parser p("{");

    auto result = p.fetch();

    ASSERT_TRUE(result->is_object());
}

TEST(json_parser_marker_test, close_curly__object_end_mark) {
    json::parser p("}");

    auto result = p.fetch();

    ASSERT_TRUE(result->is_object_end());
}

TEST(json_parser_marker_test, open_square__array_mark) {
    json::parser p("[");

    auto result = p.fetch();

    ASSERT_TRUE(result->is_array());
}

TEST(json_parser_marker_test, close_square__array_end_mark) {
    json::parser p("]");

    auto result = p.fetch();

    ASSERT_TRUE(result->is_array_end());
}
