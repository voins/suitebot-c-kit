#ifndef BOT_REQUEST_HANDLER_HH
#define BOT_REQUEST_HANDLER_HH
#include <net/request-handler.hh>
#include <suitebot/ai.hh>
#include <suitebot/deserialize.hh>
#include <suitebot/timer.hh>


namespace suitebot {
    struct parsed_request {
        int id;
        std::shared_ptr<game::state> state;
    };

    class request_handler: public net::request_handler {
        timer default_timer;
        suitebot::ai& ai;
        timer& uptime;
        bool exit = false;

    public:
        request_handler(suitebot::ai& ai): request_handler(ai, default_timer)
        {}

        request_handler(suitebot::ai& ai, timer& uptime): ai(ai), uptime(uptime)
        {
            uptime.start();
        }

        virtual ~request_handler(void) {}

        virtual std::string process(const std::string& request) override {
            if(request == "NAME")        return NAME();
            else if(request == "UPTIME") return UPTIME();
            else if(request == "EXIT")   return EXIT();

            parsed_request data = parse(request);
            return ai.make_move(data.id, data.state);
        }

        virtual bool exit_requested(void) override {
            return exit;
        }

        virtual parsed_request parse(const std::string& request) {
            suitebot::deserialize data(request);
            return {data.id(), data.state()};
        }

    private:
        std::string NAME(void) {
            return ai.name();
        }

        std::string UPTIME(void) {
            return std::to_string(uptime.elapsed());
        }

        std::string EXIT(void) {
            exit = true;
            return "";
        }
    };
}

#endif
