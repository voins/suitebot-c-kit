#ifndef SUITEBOT_AI_HH
#define SUITEBOT_AI_HH
#include <string>
#include <memory>
#include <game/state.hh>

namespace suitebot {
    struct ai {
        virtual ~ai(void) = 0;
        virtual std::string name(void) const = 0;
        virtual std::string
        make_move(int id, std::shared_ptr<game::state> state) = 0;
    };
}

#endif
