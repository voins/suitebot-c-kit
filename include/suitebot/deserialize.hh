#ifndef SUITEBOT_DESERIALIZE_HH
#define SUITEBOT_DESERIALIZE_HH
#include <json/parser.hh>
#include <game/builder.hh>
#include <game/immutable_state.hh>

namespace suitebot {
    class deserialize {
        json::parser p;
        int yourbotid = -1;
        std::string game_state;
        std::map<int, int> energy_map;
        std::unordered_set<int> bot_ids;

    public:
        deserialize(const std::string& request);
        int id(void) const { return yourbotid; }
        std::shared_ptr<game::state> state(void) const {
            game::builder b(game_state);
            for(auto p: energy_map)
                b.set_energy(p.first, p.second);
            return std::make_shared<game::immutable_state>(b);
        }
    private:
        void set_your_bot_id(std::shared_ptr<json::token>& value);
        void set_game_plan(std::shared_ptr<json::token>& value);
        void set_bot_ids(std::shared_ptr<json::token>& value);
        void set_bot_energy(std::shared_ptr<json::token>& token);
    };
}

#endif
