#ifndef SUITEBOT_TIMER_HH
#define SUITEBOT_TIMER_HH
#include <time.h>

namespace suitebot {
    struct timer {
        time_t start_time = 0;

        void start(void) { start_time = now(); }
        time_t elapsed(void) { return now() - start_time; }

    protected:
        virtual time_t now() { return time(0); }
    };
}

#endif
