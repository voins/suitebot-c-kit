#ifndef SUITEBOT_SAMPLE_AI_HH
#define SUITEBOT_SAMPLE_AI_HH
#include <suitebot/ai.hh>

namespace suitebot {
    struct sample_ai: public ai {
        virtual ~sample_ai(void) {}

        virtual std::string name(void) const { return "Sample AI"; }
        virtual std::string make_move(int, std::shared_ptr<game::state>);
    };
}

#endif
