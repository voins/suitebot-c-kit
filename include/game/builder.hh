#ifndef GAME_BUILDER_HH
#define GAME_BUILDER_HH
#include <unordered_set>
#include <map>
#include <game/point.hh>

namespace game {
    struct builder {
        int width = 0;
        int height = 0;
        std::unordered_set<int> bot_ids;
        std::unordered_set<int> live_bots;
        std::map<int, point> bots;
        std::map<int, int> energy;
        std::unordered_set<point> obstacles;
        std::unordered_set<point> treasures;
        std::unordered_set<point> batteries;

        builder() = default;
        builder(const builder&) = default;
        builder(const std::string& src, int energy = 10);

        void set_width(int width) {
            this->width = width;
        }
        void set_height(int height) {
            this->height = height;
        }
        void add_obstacle(const point& location) {
            this->obstacles.insert(location);
        }
        void add_treasure(const point& location) {
            this->treasures.insert(location);
        }
        void add_battery(const point& location) {
            this->batteries.insert(location);
        }
        void add_bot(int id, const point& location, int energy) {
            this->bot_ids.insert(id);
            this->bots.insert(std::make_pair(id, location));
            this->energy.insert(std::make_pair(id, energy));
        }
        void set_energy(int id, int energy) {
            auto r = this->energy.insert(std::make_pair(id, energy));
            if(!r.second) r.first->second = energy;
        }
    };
}

#endif
