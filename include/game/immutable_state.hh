#ifndef GAME_IMMUTABLE_STATE_HH
#define GAME_IMMUTABLE_STATE_HH
#include <map>
#include <game/state.hh>
#include <game/builder.hh>

namespace game {
    class immutable_state: public state {
        const int width;
        const int height;
        const std::unordered_set<int> bot_ids;
        const std::unordered_set<int> live_bots;
        const std::map<int, point> bots;
        const std::map<int, int> energy;
        const std::unordered_set<point> obstacles;
        const std::unordered_set<point> treasures;
        const std::unordered_set<point> batteries;

    public:
        immutable_state(const builder& src)
            : width(src.width),
              height(src.height),
              bot_ids(src.bot_ids),
              live_bots(src.live_bots),
              bots(src.bots),
              energy(src.energy),
              obstacles(src.obstacles),
              treasures(src.treasures),
              batteries(src.batteries)
        {}

        int plan_width(void) const { return width; }
        int plan_height(void) const { return height; }
        virtual const std::unordered_set<int>& all_bot_ids(void) const {
            return bot_ids;
        }
        virtual const std::unordered_set<int>& live_bot_ids(void) const {
            return live_bots;
        }
        virtual const point& bot_location(int id) const {
            auto i = bots.find(id);
            if(i == bots.end())
                throw std::runtime_error("unknown bot: "
                                         + std::to_string(id));
                return i->second;
        }
        virtual int bot_energy(int id) const {
            auto i = energy.find(id);
            if(i == energy.end())
                throw std::runtime_error("unknown bot: "
                                         + std::to_string(id));
            return i->second;
        }
        virtual const std::unordered_set<point>&
        obstacle_locations(void) const {
            return obstacles;
        }
        virtual const std::unordered_set<point>&
        treasure_locations(void) const {
            return treasures;
        }
        virtual const std::unordered_set<point>&
        battery_locations(void) const {
            return batteries;
        }
    };
}

#endif
