#ifndef GAME_POINT_HH
#define GAME_POINT_HH
#include<functional>

namespace game {
    struct point {
        const int x;
        const int y;
    };

    inline bool operator ==(const point& a, const point& b) {
        return a.x == b.x && a.y == b.y;
    }

    inline point operator +(const point& a, const point& b) {
        return point {a.x + b.x, a.y + b.y};
    }
}

namespace std {
    template <> struct hash<game::point> {
        int operator()(const game::point& data) const {
            return 31 * data.x + data.y;
        }
    };
}

#endif
