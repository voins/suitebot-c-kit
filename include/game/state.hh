#ifndef GAME_STATE_HH
#define GAME_STATE_HH
#include <list>
#include <unordered_set>
#include <game/point.hh>

namespace game {
    struct state {
        virtual ~state(void) {}
        virtual int plan_width(void) const = 0;
        virtual int plan_height(void) const = 0;
        virtual const std::unordered_set<int>& all_bot_ids(void) const = 0;
        virtual const std::unordered_set<int>& live_bot_ids(void) const = 0;
        virtual const point& bot_location(int id) const = 0;
        virtual int bot_energy(int id) const = 0;
        virtual const std::unordered_set<point>&
        obstacle_locations(void) const = 0;
        virtual const std::unordered_set<point>&
        treasure_locations(void) const = 0;
        virtual const std::unordered_set<point>&
        battery_locations(void) const = 0;
    };
}

#endif
