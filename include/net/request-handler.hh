#ifndef NET_REQUEST_HANDLER_HH
#define NET_REQUEST_HANDLER_HH
#include <string>

namespace net {
    struct request_handler {
        virtual ~request_handler(void) = 0;
        virtual std::string process(const std::string& request) = 0;
        virtual bool exit_requested(void) = 0;
    };
}

#endif
