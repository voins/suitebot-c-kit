#ifndef NET_LISTENER_HH
#define NET_LISTENER_HH
#include <net/streambuf.hh>

namespace net {
    struct listener: public socket {
        listener(int port): socket(open()) {
            set_option(SO_REUSEADDR, 1);
            set_option(SO_REUSEPORT, 1);
            bind(port);
            listen();
        }

        socketbuf accept(void) {
            return socketbuf(socket::accept());
        }
    };
}

#endif
