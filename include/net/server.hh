#ifndef NET_SERVER_HH
#define NET_SERVER_HH
#include <net/request-handler.hh>

namespace net {
    class server {
        int port;
        request_handler& handler;

    public:
        server(int port, request_handler& handler)
            : port(port), handler(handler)
        {}

        void run(void);
        bool handle(std::iostream& client);
    };
}

#endif
