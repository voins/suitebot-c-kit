#ifndef NET_SOCKET_HH
#define NET_SOCKET_HH
#include <stdexcept>
#include <cstring>
#include <unistd.h>
#include <netinet/ip.h>

namespace net {
    class socket {
        int fd;

    protected:
        socket(int fd): fd(fd) {}

        virtual ~socket() {
            ::close(fd);
        }

        static int check(int result) {
            if(result == -1)
                throw std::runtime_error(::strerror(errno));
            return result;
        }


        static int open(void) {
            return check(::socket(AF_INET, SOCK_STREAM, 0));
        }

        void set_option(int name, int value) {
            check(::setsockopt(fd, SOL_SOCKET, name, &value, sizeof(value)));
        }

        void bind(int port) {
            sockaddr_in addr {AF_INET, htons(port), {htonl(INADDR_ANY)}, {}};

            check(::bind(fd, (struct sockaddr *)&addr, sizeof(addr)));
        }

        void listen(void) {
            check(::listen(fd, 10));
        }

        int accept(void) {
            return check(::accept(fd, nullptr, nullptr));
        }

        int receive(void* buffer, size_t length) {
            return check(::recv(fd, buffer, length, 0));
        }

        int send(void *buffer, size_t length) {
            return check(::send(fd, buffer, length, 0));
        }
    };
}

#endif
