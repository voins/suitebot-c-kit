#ifndef NET_STREAMBUF_HH
#define NET_STREAMBUF_HH
#include <streambuf>
#include <vector>
#include <net/socket.hh>

namespace net {
    class socketbuf: public std::streambuf, socket {
        std::vector<char> inbuf;
        std::vector<char> outbuf;


        virtual int_type underflow(void) {
            if(gptr() == egptr()) {
                int count = receive(&inbuf[0], inbuf.size());
                setg(&inbuf[0], &inbuf[0], &inbuf[0] + count);

                if(count == 0)
                    return traits_type::eof();
            }
            return traits_type::to_int_type(*gptr());
        }


        virtual int_type overflow(int_type c) {
            if(c != traits_type::eof()) {
                *pptr() = traits_type::to_char_type(c);
                pbump(1);
            }
            sync();
            return c;
        }


        virtual int_type sync(void) {
            while(pptr() > pbase()) {
                int count = send(pbase(), pptr() - pbase());
                setp(pbase() + count, pptr());
            }
            setp(&outbuf[0], &outbuf[0] + outbuf.size() - 1);
            return 0;
        }


    public:
        socketbuf(int fd)
            : socket(fd), inbuf(65536), outbuf(65536)
        {
            setg(&inbuf[0], &inbuf[0], &inbuf[0]);
            setp(&outbuf[0], &outbuf[0] + outbuf.size() - 1);
        }
    };
}

#endif
