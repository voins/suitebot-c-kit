#ifndef PARSER_BASE_HH
#define PARSER_BASE_HH
#include <string>

namespace parser {
    class base {
        std::string json;
        std::string::size_type pos = 0;

    protected:
        base(std::string json, std::string::size_type pos = 0)
            : json(json), pos(pos) {}

        bool atend(void) {
            return pos >= json.size();
        }

        void advance(std::string::size_type num = 1) {
            if(pos + num < json.size())
                pos += num;
            else
                pos = json.size();
        }

        char current(void) {
            return json[pos];
        }

        bool in(const std::string& set) {
            return !atend() && set.find(current()) != std::string::npos;
        }

        bool is(char c) {
            return !atend() && current() == c;
        }

        bool is(const std::string& str) {
            return json.compare(pos, str.size(), str) == 0;
        }

        bool isxdigit() {
            return !atend() && ::isxdigit(current());
        }

        bool isdigit() {
            return !atend() && ::isdigit(current());
        }

        void skip_whitespace(void) {
            while(in(" \t\r\n:,"))
                advance();
        }
    };
}

#endif

