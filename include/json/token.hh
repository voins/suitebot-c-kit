#ifndef JSON_TOKEN_HH
#define JSON_TOKEN_HH
#include <string>

namespace json {
    struct token {
        virtual ~token(void) {}

        virtual bool is_string(void) const { return false; }
        virtual std::string as_string(void) const { return ""; }
        virtual bool is_integer(void) const { return false; }
        virtual int as_integer(void) const { return 0; }
        virtual bool is_double(void) const { return false; }
        virtual double as_double(void) const { return 0; }
        virtual bool is_boolean(void) const { return false; }
        virtual bool as_boolean(void) const { return false; }
        virtual bool is_array(void) const { return false; }
        virtual bool is_array_end(void) const { return false; }
        virtual bool is_object(void) const { return false; }
        virtual bool is_object_end(void) const { return false; }
        virtual bool is_end(void) const { return false; }
        virtual bool is_null(void) const { return false; }
    };

    struct string_token: public token {
        std::string value;

        explicit string_token(const std::string& value): value(value) {}

        virtual bool is_string(void) const { return true; }
        virtual std::string as_string(void) const { return value; }
    };

    struct integer_token: public token {
        long long value;

        explicit integer_token(long long value): value(value) {}

        virtual bool is_integer(void) const { return true; }
        virtual int as_integer(void) const { return value; }
    };

    struct double_token: public token {
        double value;

        explicit double_token(double value): value(value) {}

        virtual bool is_double(void) const { return true; }
        virtual double as_double(void) const { return value; }
    };

    struct boolean_token: public token {
        bool value;

        explicit boolean_token(bool value): value(value) {}

        virtual bool is_boolean(void) const { return true; }
        virtual bool as_boolean(void) const { return value; }
    };

    struct null_token: public token {
        virtual bool is_null(void) const { return true; }
    };

    struct mark_token: public token {
        char mark;

        explicit mark_token(char mark): mark(mark) {}

        virtual bool is_array(void) const { return mark == '['; }
        virtual bool is_array_end(void) const { return mark == ']'; }
        virtual bool is_object(void) const { return mark == '{'; }
        virtual bool is_object_end(void) const { return mark == '}'; }
    };

    struct end_token: public token {
        virtual bool is_end(void) const { return true; }
        virtual bool is_object_end(void) const { return true; }
        virtual bool is_array_end(void) const { return true; }
    };
}

#endif
