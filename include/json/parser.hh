#ifndef JSON_PARSER_HH
#define JSON_PARSER_HH
#include <json/base.hh>
#include <json/token.hh>
#include <memory>

namespace json {
    struct parser: public ::parser::base {
        parser(std::string json, std::string::size_type pos = 0)
            : ::parser::base(json, pos) {}

        std::shared_ptr<token> fetch(void);
        void skip(std::shared_ptr<token> t);

    private:
        [[noreturn]] void throw_error(void);
        char parse_escape(void);
        char parse_hexcode(void);
        void parse_word(const std::string& word);
        std::shared_ptr<token> parse_string(void);
        std::shared_ptr<token> parse_number(void);
        std::shared_ptr<token> parse_mark(void);
        std::shared_ptr<token> parse_true(void);
        std::shared_ptr<token> parse_false(void);
        std::shared_ptr<token> parse_null(void);
    };
}

#endif

