#ifndef TEST_TIMER_HH
#define TEST_TIMER_HH
#include <gmock/gmock.h>
#include <suitebot/timer.hh>

namespace fake {
    struct timer: public suitebot::timer {
        MOCK_METHOD0(now, time_t());
    };
}

#endif
