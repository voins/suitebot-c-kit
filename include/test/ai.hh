#ifndef TEST_AI_HH
#define TEST_AI_HH
#include <gmock/gmock.h>
#include <suitebot/ai.hh>

namespace fake {
    struct ai: public suitebot::ai {
        MOCK_CONST_METHOD0(name, std::string());
        MOCK_METHOD2(make_move,
                     std::string(int, std::shared_ptr<game::state>));
    };
}

#endif

